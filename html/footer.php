	<div class="footer_placeholder"></div>
	<footer class="footer">
		<div class="container">
			<div class="leftMenu_w">
				<div class="fSocial">
					<div class="fSocial-title">МЫ В СЕТИ:</div>
					<div class="fSocial-icons">
						<a href="#" class="sprite icon sprite-icon_tv_gr"></a>
						<a href="#" class="sprite icon sprite-icon_vk_gr"></a>
						<a href="#" class="sprite icon sprite-icon_fb_gr"></a>
						<a href="#" class="sprite icon sprite-icon_gp_gr"></a>
						<a href="#" class="sprite icon sprite-icon_od_gr"></a>
						<a href="#" class="sprite icon sprite-icon_inst_gr"></a>
					</div>
				</div>
			</div>
			<div class="content_w">
				<div class="content">
					<ul class="footer-menu">
						<li><a href="#"><span class="sprite icon sprite-icon_deliv"></span>Доставка</a></li>
						<li><a href="#"><span class="sprite icon sprite-icon_food_lit"></span>Рецепты</a></li>
						<li><a href="#"><span class="sprite icon sprite-icon_cam_lit"></span>Видео</a></li>
						<li><a href="#"><span class="sprite icon sprite-icon_list_lit"></span>Новости</a></li>
						<li><a href="#"><span class="sprite icon sprite-icon_pic"></span>Контакты</a></li>
						<li><a href="#"><span class="sprite icon sprite-icon_prof"></span>О нас</a></li>
					</ul>
					<div class="footer-bottom">
						<div class="footer-right">
							<div class="fContacts">
								<div class="fContacts-row">
									<i class="sprite icon sprite-icon_phone_gr"></i>(495)755-45-10
								</div>
								<div class="fContacts-row">
									<a href="#"><i class="sprite icon sprite-icon_mess_gr"></i>info@shop.rusfds.ru</a>
								</div>
							</div>
						</div>
						<div class="footer-left">
							<div class="yandexReting">
								<a href="#"><img src="images/retingya.jpg" alt="" /></a>
							</div>
						</div>
						<div class="footer-center">
							<div class="payInfo">
								<div class="payInfo-title">Способы оплаты:</div>
								<img src="images/pay.png" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?partial('head_bootstrap')?>
</body>
</html>