	<div class="container main">
		<div style="background: #ccc;">
			<i class="sprite icon sprite-icon_bdate"></i>
			<i class="sprite icon sprite-icon_cam_lit"></i>
			<i class="sprite icon sprite-icon_camera_blue"></i>
			<i class="sprite icon sprite-icon_cel"></i>
			<i class="sprite icon sprite-icon_date"></i>
			<i class="sprite icon sprite-icon_deliv"></i>
			<i class="sprite icon sprite-icon_fb_blue"></i>
			<i class="sprite icon sprite-icon_fb_gr"></i>
			<i class="sprite icon sprite-icon_food"></i>
			<i class="sprite icon sprite-icon_food_lit"></i>
			<i class="sprite icon sprite-icon_gp_blue"></i>
			<i class="sprite icon sprite-icon_gp_gr"></i>
			<i class="sprite icon sprite-icon_hum"></i>
			<i class="sprite icon sprite-icon_inst_blue"></i>
			<i class="sprite icon sprite-icon_inst_gr"></i>
			<i class="sprite icon sprite-icon_key"></i>
			<i class="sprite icon sprite-icon_ko"></i>
			<i class="sprite icon sprite-icon_list"></i>
			<i class="sprite icon sprite-icon_list_lit"></i>
			<i class="sprite icon sprite-icon_mess_gr"></i>
			<i class="sprite icon sprite-icon_od_blue"></i>
			<i class="sprite icon sprite-icon_od_gr"></i>
			<i class="sprite icon sprite-icon_phone_gr"></i>
			<i class="sprite icon sprite-icon_phonew"></i>
			<i class="sprite icon sprite-icon_pic"></i>
			<i class="sprite icon sprite-icon_prof"></i>
			<i class="sprite icon sprite-icon_search"></i>
			<i class="sprite icon sprite-icon_timer"></i>
			<i class="sprite icon sprite-icon_tv_blue"></i>
			<i class="sprite icon sprite-icon_tv_gr"></i>
			<i class="sprite icon sprite-icon_vk_blue"></i>
			<i class="sprite icon sprite-icon_vk_gr"></i>
			<i class="sprite icon sprite-icon_weght"></i>
		</div>
		<div class="buttons" style="background-color:#fff;">
			<a href="#" class="btn btn-default btn-lg">btn-default btn-lg</a>
			<a href="#" class="btn btn-default">btn btn-default</a>
			<a href="#" class="btn btn-default btn-md">btn-default btn-md</a>
			<a href="#" class="btn btn-default btn-xs">btn-default btn-xs</a>
		</div>
		<div class="buttons" style="background-color:#fff;">
			<a href="#" class="btn btn-primary btn-lg">btn btn-primary btn-lg</a>
			<a href="#" class="btn btn-primary">btn btn-primary</a>
			<a href="#" class="btn btn-primary btn-md">btn btn-primary btn-md</a>
			<a href="#" class="btn btn-primary btn-xs">btn btn-primary btn-xs</a>
		</div>
		<div class="buttons" style="background-color:#fff;">
			<a href="#" class="btn btn-success btn-lg">btn btn-success btn-lg</a>
			<a href="#" class="btn btn-success">btn btn-default</a>
			<a href="#" class="btn btn-success btn-md">btn btn-success btn-md</a>
			<a href="#" class="btn btn-success btn-xs">btn btn-success btn-xs</a>
		</div>
		<div class="buttons" style="background-color:#fff;">
			<a href="#" class="btn btn-info btn-lg">btn btn-info btn-lg</a>
			<a href="#" class="btn btn-info">btn-default</a>
			<a href="#" class="btn btn-info btn-md">btn btn-info btn-md</a>
			<a href="#" class="btn btn-info btn-xs">btn btn-info btn-xs</a>
		</div>
		<div class="buttons" style="background-color:#fff;">
			<a href="#" class="btn btn-danger btn-lg">btn btn-danger btn-lg</a>
			<a href="#" class="btn btn-danger">btn btn-default</a>
			<a href="#" class="btn btn-danger btn-md">btn btn-danger btn-md</a>
			<a href="#" class="btn btn-danger btn-xs">btn btn-danger btn-xs</a>
		</div>
	</div>
