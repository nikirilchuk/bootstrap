/***
**To run Grunt you need node.js and grunt.
**To install grunt, type in console "npm install -g grunt-cli"
**If dir "node_modules" is absent, to install packages type in console "npm install"
**To put together scripts type in console "grunt scripts".
***/

module.exports = function(grunt){
	grunt.initConfig({ 
		pkg: grunt.file.readJSON('package.json'),
		concat:{
			options:{
				separator: ';\n'
			},
			dist:{
				src: [
					'js/jquery.min.js', 
					'js/jquery.browser.min.js',
					'js/bootstrap/bootstrap.min.js',
					'js/selectivizr-min.js', 
					'js/jquery.watermark.min.js',
					'js/jquery.columnizer.js',
					'fancybox/jquery.fancybox.js',
					'js/jquery.validate.min.js', 
					'js/jquery.znice.validate.js', 
					'js/jquery.znice.js', 
					'js/modernizr.js',
					'js/maskedinput.js',
					'js/slick.js',
					'js/flipclock.min.js',
					'js/scr.js',
				 ],
				dest: 'js/general.js'
			}
		},
		uglify:{
			js:{
				files:{
					'js/general.js': ['js/general.js']
				}
			}
		}
	});

	// grunt.initConfig({ 
	// 	pkg: grunt.file.readJSON('package.json'),
	// 	concat:{
	// 		options:{
	// 			separator: ';\n'
	// 		},
	// 		dist:{
	// 			src: [
	// 				'js_m/jquery.min.js', 
	// 				'js_m/jquery.browser.min.js',
	// 				'js_m/bootstrap/bootstrap.min.js',
	// 				'js_m/jquery.watermark.min.js',
	// 				'fancybox/jquery.fancybox.js',
	// 				'js_m/jquery.validate.min.js', 
	// 				'js_m/jquery.znice.validate.js', 
	// 				'js_m/jquery.znice.js', 
	// 				'js_m/maskedinput.js',
	// 				'js_m/jquery.mobile.custom.min.js',
	// 				'js_m/slick.js',
	// 				'js_m/scr.js',
	// 			 ],
	// 			dest: 'js_m/general.js'
	// 		}
	// 	},
	// 	uglify:{
	// 		js:{
	// 			files:{
	// 				'js_m/general.js': ['js_m/general.js']
	// 			}
	// 		}
	// 	}
	// });
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	
	grunt.registerTask('default', ['concat', 'uglify']);
};