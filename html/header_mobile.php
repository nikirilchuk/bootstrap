<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no"/>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css_m/style.css" media="all" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Exo+2&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
</head>
<body>
	<?
		global $act;
		partial('zHiddenBlock');
	?>
	<div class="hBasket-inner">
		<div class="hBasket-inner-scroll">
			<?partial('hBasketElems');?>
		</div>
		<div class="hBasket-inner-text">
			В корзине <span>3</span> товара на сумму: <span>3255 р</span>
		</div>
		<div class="hBasket-buttons">
			<a href="#" class="btn btn-md btn-default">Просмотр корзины</a>
			<a href="#" class="btn btn-md btn-primary">ОФОРМЛЕНИЕ ЗАКАЗА</a>
			<div class="juster"></div>
		</div>
	</div>
	<div class="container">
		<?partial('leftMenu_mobile');?>
		<header class="header">
			<a href="#" class="header-top text-center">
				<span class="icon icon-screen"></span> ПЕРЕЙТИ НА полную ВЕРСИЮ
			</a>
			<div class="header-middle">
				<div class="header-middle-left">
					<div class="header-logo">
						<a href="/"><img src="images_m/hlogo.png" alt=""/></a>
					</div>
				</div>
				<div class="header-middle-right">
					<div class="hPhone">
						(495)755-45-10
					</div>
					<a href="#recallForm" class="button btn-info fancybox-popup">
						<span class="icon icon-mob icon_wphone"></span> Заказать звонок
					</a>
					<div class="hTime">
						ПН-ПТ с  9.30 до 23.00
					</div>
				</div>
			</div>
			<div class="header-menu">
				<ul>
					<li>
						<a href="#" class="menuToggle">
							<span class="icon icon-mob icon_menu"></span><span class="vfix"></span>
						</a>
					</li>
					<li>
						<a href="#" class="searchToggle">
							<span class="icon icon-mob icon_hsearch"></span><span class="vfix"></span>
						</a>
					</li>
					<li>
						<a href="#" class="logToggle">
							<span class="icon icon-mob icon_proff"></span><span class="vfix"></span>
						</a>
					</li>
					<li>
						<a href="#" class="hBasket-top">
							<span class="icon icon-mob icon_bask"></span><span class="vfix"></span>
						</a>
					</li>
				</ul>
			</div>
			<div class="zSearch zNice">
				<form>
					<div class="zSearch-input">
						<input type="text" required="required" placeholder="Поиск по сайту" />
					</div>
					<div class="zSearch-submit">
						<input type="submit" value=""/>
					</div>
				</form>
			</div>
			<div class="header-links">
				<a href="#"><span class="icon icon-mob icon_key"></span>Регистрация</a>
					<div class="hsep"></div>
					<a href="#loginForm" class="fancybox-popup"><span class="icon icon-mob icon_hum"></span>Войти</a>
			</div>
		</header>
