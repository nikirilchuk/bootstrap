	<?include 'header.php';?>
	<div class="productPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<div class="clearfix"></div>
						<div class="productTop">
							<div class="productTop-left">
								<?partial('productGallery');?>
							</div>
							<div class="productTop-right">
								<?partial('productDescription');?>
							</div>
						</div>
						<div class="thirdheading">
							<span>ИНФОРМАЦИЯ О ТОВАРЕ:</span>
						</div>
						<?partial('infoProduct');?>
						<div class="thirdheading">
							<span>РЕКОМЕНДУЕМЫЕ ТОВАРЫ</span>
						</div>
						<div class="prodCarousel">
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
							</div>
						</div>
						<div class="thirdheading">
							<span>Отзывы</span><a href="#">Все отзывы</a>
						</div>
						<?partial('reviewBlock');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>