	<?include 'header_mobile.php';?>
	<div class="newsPage innerPage">
		<div class="container main">
			<div class="row">
				<h3 class="text-center">
					НОВОСТИ
				</h3>
				<?partial('news-item_m');?>
				<?partial('paginator');?>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>