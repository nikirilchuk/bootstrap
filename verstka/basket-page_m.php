<?include 'header_mobile.php';?>
<div class="basketPage innerPage">
	<div class="container main">
		<div class="row">
			<h3 class="text-center">
				КОРЗИНА ПОКУПОК
			</h3>
			<div class="innerContent">
				<div class="zForm checkout checkoutStep-four zNice">
					<form action="ajax.php">
						<div class="completePopup-text" data-title="Заказ выполнен" data-text="Заказ будет обработан в ближайшее время"></div>
						<div class="basketForm">
							<div class="basketForm-item basketForm-body text-center">
								<a href="#" class="basketForm-remove icon"><img src="images_m/basket_remove.png" alt=""/></a>
								<div class="basketProdItem-img">
									<a href="#"><img src="images_m/product_img.png" alt=""/></a></div>
								<a href="#" class="basketProdItem-title">КРАСНАЯ ИКРА КЕТЫ<br/>  "ИТА СЕВЕРНАЯ КОМПАНИЯ"<br/> СТ\Б, 114Г.
								</a>
								<div class="basketForm-item-bottom">
									<div class="basketForm-item-col">
										<div class="basketProdItem-count">
											<input type="text" data-ztype="qInput" class="rightButton" value="1" data-price="650" />
										</div>
									</div>
									<div class="basketForm-item-col">
										<div class="basketProdItem-count">
											<div class="basketProdItem-price rowPrice" data-rowprice="650"><span>650</span> p.</div>
										</div>
									</div>
								</div>
							</div>
							<div class="basketForm-item basketForm-body text-center">
								<a href="#" class="basketForm-remove icon"><img src="images_m/basket_remove.png" alt=""/></a>
								<div class="basketProdItem-img">
									<a href="#"><img src="images_m/product_img.png" alt=""/></a></div>
								<a href="#" class="basketProdItem-title">КРАСНАЯ ИКРА КЕТЫ<br/>  "ИТА СЕВЕРНАЯ КОМПАНИЯ"<br/> СТ\Б, 114Г.
								</a>
								<div class="basketForm-item-bottom">
									<div class="basketForm-item-col">
										<div class="basketProdItem-count">
											<input type="text" data-ztype="qInput" class="rightButton" value="1" data-price="650" />
										</div>
									</div>
									<div class="basketForm-item-col">
										<div class="basketProdItem-price rowPrice" data-rowprice="650"><span>650</span> p.</div>
									</div>
								</div>
							</div>
						</div>
						<div class="basketForm-bottom">
							<div class="zForm-inner mlForm basketDiscont-form">
								<div class="zForm-row">
									<label>
										<input type="radio" name="text1" checked="checked" class="cuponeDiscont"/>  <span class="zForm-text">Использовать купон на скидку </span>
									</label>
								</div>
								<div class="zForm-row">
									<label>
										<input type="radio" name="text1"/>  <span class="zForm-text">Использовать подарочный сертификат </span>
									</label>
								</div>
								<div class="zForm-row dibi">
									<input type="text" class="numberInput redColor" value="3213-2132-1352-1321"/>
								</div>
								<div class="basketForm-discont" data-discontcart="10" data-discontsert="500">У Вас <span>10%</span> скидки!</div>
							</div>
							<div class="basketAllprice text-right">
								<div class="basketAllprice-row">
									Сумма <span class="basketAllprice-sum"><span>1300</span>р</span>
								</div>
								<div class="basketAllprice-row redColor">
									Скидка <span class="basketAllprice-discont" ><span>130</span>р</span>
								</div>
								<div class="basketAllprice-row">
									Итого <span class="basketAllprice-basket"><span>1170</span>р</span>
								</div>
							</div>
						</div>
						<div class="zForm-row text-center buttonRow">
							<input type="submit" class="btn-primary btn-md" value="ОФОРМЛЕНИЕ ЗАКАЗА" />
						</div>
					</form>
				</div>
			</div>
			<div class="thirdheading">
				<span>РЕКОМЕНДУЕМЫЕ ТОВАРЫ</span>
			</div>
			<div class="prodCarousel">
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
				<div class="prodCarouselitem">
					<?partial('productItem');?>
					<?partial('productItem');?>
				</div>
			</div>
		</div>
	</div>
</div>
<?include 'footer_mobile.php';?>