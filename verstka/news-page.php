	<?include 'header.php';?>
	<div class="newsPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							НОВОСТИ
						</h3>
						<?partial('news-item');?>
						<?partial('paginator');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>