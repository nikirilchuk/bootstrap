	<?include 'header_mobile_index.php';?>
	<div class="main">
		<div class="row">
			<div class="catalogItems-wrap">
				<div class="thirdheading">
					<span>ТОВАРЫ НЕДЕЛИ</span>
				</div>
				<div class="catalogItems">
					<div class="catalogItems-col">
						<?partial('productItem01');?>
					</div>
					<div class="catalogItems-col">
						<?partial('productItem');?>
					</div>
				</div>
				<div class="text-center">
					<a href="#" class="slink">Все товары</a>
				</div>
			</div>
			<div class="catalogItems-wrap">
				<div class="catalogItems-col">
					<?partial('productItem01');?>
				</div>
				<div class="catalogItems-col">
					<?partial('productItem');?>
				</div>
				<div class="text-center">
					<a href="#" class="slink">Все акции</a>
				</div>
			</div>
			<div class="infoBlocks">
				<div class="newsBlock">
					<div class="thirdheading">
						<span>НОВОСТИ</span>
					</div>
					<div class="innerContent">
						<?partial('newsBlock-item')?>
						<?partial('newsBlock-item')?>
						<div class="text-center">
							<a href="#" class="slink">Все новости</a>
						</div>
					</div>
				</div>
				<div class="recBlock">
					<div class="thirdheading">
						<span>РЕЦЕПТЫ</span>
					</div>
					<div class="innerContent">
						<?partial('receptItem')?>
						<div class="text-center">
							<a href="#" class="slink">Все рецепты</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>