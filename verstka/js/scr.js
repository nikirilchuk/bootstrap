(function($) {


	var scroller=jQuery.browser.webkit ? "body": "html";

	/* modernize */
	function modernize() {
		// placeholder 
		if(!Modernizr.input.placeholder){
			$('[placeholder]').each(function() {
				$(this).watermark($(this).attr('placeholder'));
			});
		}
	}

	function inputMask(input, mask){
		input.each(function(){
			$(this).mask(mask);
			maskSubmitFix(input);
		});
	};

	function maskSubmitFix(input){
		input.focusout(function(){
		var masktext=$(this).val();
		if(masktext.indexOf("_")!=(-1)) $(this).val("");
		});
	};

	function formReset(form){
		var element = form.find('input,textarea').not('input[type="submit"], input[type="hidden"]'),
			classes = form.find('.zNice-error,.zNice-valid');
		element.val("");
		classes.removeClass('zNice-error zNice-valid');
		$('.required-field>span:last-child').removeClass('hidden');
	}

	/** String formatter allows to do like this
	/* "{0} is dead, but {1} is alive! {0} {2}".format("ASP", "ASP.NET")
	**/
	if (!String.prototype.format) {
	    String.prototype.format = function() {
	        var args = arguments;
	        return this.replace(/{(\d+)}/g, function(match, number) {
	            return typeof args[number] != 'undefined' ? args[number] : match;
	        });
	    };
	 }

	 //USAGE: $("#form").serializefiles();
	$.fn.serializefiles = function() {
	    var obj = $(this);
	    /* ADD FILE TO PARAM AJAX */
	    var formData = new FormData();
	    $.each($(obj).find("input[type='file']"), function(i, tag) {
	        $.each($(tag)[0].files, function(i, file) {
	            formData.append(tag.name, file);
	        });
	    });
	    var params = $(obj).serializeArray();
	    $.each(params, function (i, val) {
	        formData.append(val.name, val.value);
	    });
	    return formData;
	};

	function slide_UpDown(){
		var open_close=$('.productList-button-read');

		open_close.click(function(){
			var block = $(this).closest('.productList'),//внешняя обгортка
				hidden = block.find('.productList-content-text');//скрытый блок
			if(block.is('.active')){
				block.removeClass('active');
				hidden.slideUp();
				$(this).html('Читать полностью');
			}
			else{
				block.addClass('active');
				hidden.slideDown();
				$(this).html('Скрыть');
			}
			return false;
		});
	};
	/* scrollUp */
	function scrollUp(block,targetBlock) {
		$(block).click(function(e){
			var target = $(targetBlock).offset().top;
			$(scroller).animate({scrollTop:target},800);
			return false;
			e.preventDefault();
		});
	}

	var allPriceBasket;

	function discontFunc(summ){
		var discontBlock = $('.basketForm-discont'),
			discont,
			discontPrice;
		if($('.cuponeDiscont input').is(':checked')){
			discont = discontBlock.attr('data-discontcart');
			discontPrice = summ*discont/100;
		}
		else{
			discont = discontBlock.attr('data-discontsert');
			discontPrice = discont;
		}
		allPriceBasket = summ-discontPrice;
		$('.basketAllprice-discont span').html(discontPrice);
	}

		/* basket */
	function allPrice(){
		var total_price = 0;

		$('.rowPrice').each(function() {
			var price_tmp = $(this).attr('data-rowPrice');
			total_price = total_price + price_tmp*1;
		});

		discontFunc(total_price);
		$('.basketAllprice-sum span').html(total_price);
		$('.basketAllprice-basket span').html(allPriceBasket);
	}

	function BasketSumm(elem){
		var parent = elem.closest('.basketForm-body'),
			product_price = elem.attr('data-price'),
			amount = elem.val(),
			total_product_price = product_price*1 * amount*1;
		$('.rowPrice', parent).attr( 'data-rowprice', total_product_price );
		$('.rowPrice span', parent).html(total_product_price);
		allPrice();
	}

	function removeItem(){
		$('.basketForm-remove').click(function(){
			$(this).closest('.basketForm-body').remove();
			allPrice();
			if($('.basketForm-body').length==0){
				$('.checkoutStep-four').html('<h4 class="text-center" style="padding-top:50px;">Корзина пуста</h4>');
			}
			return false;
		})
	}
	/* basket */

	function zForm(){
		$('.zNice form').submit(function(e){
			var form = $(this);
			if(form.valid()){
			    e.stopPropagation();
			    e.preventDefault();
			    $.ajax({
			        type : 'POST',
			        url : $(this).attr('action'),
			        data : $(this).serializefiles(),
			        processData: false,
			        contentType: false,
			        success : function(data){
			        	if(form.closest('.jsForm').length>0){
				            var textMessage = form.find('.completePopup-text'),
				        		messageTitle = textMessage.attr('data-title'),
				        		messageText = textMessage.attr('data-text'),
				        		formMessage = $('#formMessage'),
				        		formMessageTitle = formMessage.find('.zForm-title'),
				        		formMessageText = formMessage.find('.zForm-subtitle');
				        	if(typeof(messageTitle)!='undefined' || messageText !='undefined'){
					        	formMessageTitle.html(messageTitle);
								formMessageText.html(messageText);
				        	}
				        	$.fancybox.open({
				        		content: formMessage,
				        		padding:0
				        	});
			        	}
			        	else{
			        		alert('Form submit js/scr.js');
			        	}
				        formReset(form);
			        }
			    });
			    return false;
			}
		});
	}


	/*Podderzhka Placeholderov v starih brouzerah*/
	$(document).ready(function(){
		modernize();
		$('.footer_placeholder').height($('.footer').outerHeight());
		inputMask($('.phoneMask input'), "+7(999)-999-99-99");
		inputMask($('.numberInput input'), "9999-9999-9999-9999");
		slide_UpDown();
		zForm();
		removeItem();

		$('.productPrice input').change(function(){
			var price = $(this).attr('data-price');
			var numb = $(this).val();
			var price = price*numb;
			$('.basketProdItem-price span').html(price);
		})

		$('.basketProdItem-count input').change(function() {
			BasketSumm($(this))
		});

		$('.basketDiscont-form input').change(function(){
			$('.basketProdItem-count input').each(function(){
				BasketSumm($(this));
			});
			var summ = $('.basketAllprice-sum span').html()*1;
			discontFunc(summ)
		});

		$('.basketProdItem-count input').each(function(){
			BasketSumm($(this));
		});

		$('.basketDiscont-form input[type="radio"]').change(function(){
			var discont;
			var html;
			var discontBlock = $('.basketForm-discont');
			if($('.cuponeDiscont input').is(':checked')){
				discont = discontBlock.attr('data-discontcart');
				html=discont+'%';
			}
			else{
				discont = discontBlock.attr('data-discontsert');
				html=discont+'р';
			}
			$('.basketForm-discont span').html(html);
		});

		$('.reviewBlock-button').click(function(){
			$(this).hide();
			$('.reviewBlock-form').slideDown(200);
			return false;
		});
		$('.zForm').each(function(){
			var form = $(this);
			$('[type="password"]',form).each(function(){
				if($(this).is("#re-passwordo")==true){
					$(this).rules("add", {
						equalTo:form.find('#passwordo')
					});
				}
				$(this).rules( "add", {
					minlength : 6
				});
			});
		});
			
		
		$('.fancybox-popup').fancybox({
			padding:0,
			autoSize:true,
			fitToWiew:false
		});

		$('.hSlider').slick({
			dots:true,
			arrows:false
		});

		$('.vertivalCariusel-inner').slick({
			vertical:true,
			dots:false,
			arrows:true,
			slidesToShow: 2,
  			slidesToScroll: 2
		});

		$('.prodCarousel').slick({
			dots:false,
			slidesToShow: 5,
  			slidesToScroll: 5
		});

		
		$('.hBasket-top').click(function(){
			$('.hBasket-inner').toggleClass('active');
			return false;
		});

		$('.toogleBlock').click(function(){
			var el = $(this).attr('data-block');
			$(this).is('.active') ? $(this).removeClass('active').text('Скрыть') : $(this).addClass('active').text('Показать');
			$('.'+el).toggle();

			return false;
		});

		$('.sortingPanel-left a').click(function(){
			var button = $(this);
			var parent = $(this).closest('.sortingPanel-filt');
			$('.sortingPanel-filt').removeClass('active');
			parent.addClass('active');
			if(button.is('.sortingPanel-line')){
				$('.productList-itemBlock').addClass('productItem-line');
			}
			else{
				$('.productList-itemBlock').removeClass('productItem-line');
			}
			return false;
		});
		 $('.slider-for').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false,
			fade: true,
			asNavFor: '.slider-nav'
		});
		$('.slider-nav').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			asNavFor: '.slider-for',
			dots: false,
			focusOnSelect: true
		});

		$(document).click(function (e) {
		    var container = $(".hBasket-inner");
		    if (container.has(e.target).length === 0){
		        $('.hBasket').removeClass('active');
		    }
		});

		$('.fancybox-video').click(function(){
			var videoLink = $(this).attr('data-video');
			var iframe = '<iframe width="100%" height="299" src="https://www.youtube.com/embed/'+videoLink+'" frameborder="0" allowfullscreen></iframe>';
			var block = $('#videoPopup');
			$('.videoIframe').html(iframe);
			$.fancybox.open({
				content:block,
				padding:0,
				fitToWiew:false,
				autoSize:true
			});
			return false;
		})
		
	});

})(jQuery);
