<?include 'header.php';?>
<div class="checkoutPage innerPage">
	<div class="container main">
		<div class="row">
			<?partial('leftMenu');?>
			<div class="content_w">
				<div class="content">
					<?partial('breadcrumbs');?>
					<h3 class="text-center">
						ОФОРМЛЕНИЕ ЗАКАЗА
					</h3>
					<div class="zForm checkout checkoutStep-two zNice">
						<div class="zForm-head">
							Шаг 2: Профиль & Платежная информация
						</div>
						<form action="ajax.php">
							<div class="zForm-row">
								<div class="checkout-col">
									<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
									<div class="zForm-inner mlForm">
										<div class="zForm-row">
											<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
										</div>
										<div class="zForm-row">
											<input type="text" name="text2" placeholder="Фамилия" required="required"/>
										</div>
										<div class="zForm-row">
											<input type="email" name="email" placeholder="E-Mail" required="required"/>
										</div>
										<div class="zForm-row">
											<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
										</div>
									</div>
								</div>
								<div class="checkout-col">
									<div class="zForm-title">Адрес</div>
									<div class="zForm-inner mlForm">
										<div class="zForm-row">
											<div class="zForm-labtext">
												Доставка за пределы  МКАД будет 1км=30руб.
											</div>
											<select>
												<option value="0">Московская область</option>
												<option value="1">Московская область</option>
												<option value="2">Московская область</option>
												<option value="3">Московская область</option>
												<option value="4">Московская область</option>
												<option value="5">Московская область</option>
											</select>
										</div>
										<div class="zForm-row">
											<select>
												<option value="0">Москова</option>
												<option value="1">Москова</option>
												<option value="2">Москова</option>
												<option value="3">Москова</option>
												<option value="4">Москова</option>
												<option value="5">Москова</option>
											</select>
										</div>
										<div class="zForm-row">
											<input type="text" name="address" placeholder="Адрес" required="required"/>
										</div>
									</div>
								</div>
							</div>
							<div class="zForm-row text-right buttonRow">
								<input type="submit" class="btn-primary btn-md" value="Продолжить" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?include 'footer.php';?>