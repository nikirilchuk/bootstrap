<div class="leftMenu_w">
	<div class="zSearch zNice">
		<form>
			<div class="zSearch-input">
				<input type="text" required="required" placeholder="Поиск по сайту" />
			</div>
			<div class="zSearch-submit">
				<input type="submit" value=""/>
			</div>
		</form>
	</div>
	<div class="leftMenu">
		<div class="mLogo">
			<img src="images/menulogo.png" alt=""/>
		</div>
		<ul class="sMenu">
			<li>
				<a href="#">ДРУГАЯ ИКРА</a>
			</li>
			<li>
				<a href="#">МОРЕПРОДУКТЫ</a>
			</li>
			<li>
				<a href="#">КРАСНАЯ ИКРА</a>
			</li>
			<li>
				<a href="#">ЧЕРНАЯ ИКРА</a>
			</li>
			<li>
				<a href="#">РЫБА</a>
			</li>
		</ul>
	</div>
	<div class="vertivalCariusel">
		<div class="vertivalCariusel-title">
			Рекомендованные товары
		</div>
		<div class="vertivalCariusel-inner">
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
			<div class="vertivalCariusel-item">
				<div class="productItem">
					<div class="productItem-top">
						<div class="productItem-top-imag">
							<img src="images/productimg.jpg" alt="" /><span class="vfix"></span>
						</div>
						<div class="productItem-top-text">
							КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
						</div>
					</div>
					<div class="productItem-bottom">
						<div class="productItem-bottom-price">
							657 p.
						</div>
						<div class="productItem-bottom-button">
							<a href="#" class="btn btn-default"><span class="icon sprite sprite-icon_ko"></span>В КОРЗИНУ</a>
							<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>