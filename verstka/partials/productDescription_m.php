<div class="productDescription">
	<div class="zForm zNice">
		<div class="basketForm-col">
			<div class="basketProdItem-price"><span>650</span> p.</div>
		</div>
		<div class="basketForm-col">
			<div class="basketProdItem-count">
				<input type="text" data-ztype="qInput" class="leftButton rightButton" value="1" data-price="650" />
			</div>
		</div>
	</div>
	<div class="productDescription-buttons">
		<a href="#" class="btn btn-primary">Заказать</a>
		<a href="#" class="btn btn-success">В один клик</a>
	</div>
	<div class="productDescription-table">
		<table>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_cel"><img src="images/icon_cel.png" alt=""></span>
			        	</div>
			        	<div class="tableText-center">
			        		Температура хранения
			        	</div>
			        	<div class="tableText-blue">
			        		0-4 с
			        	</div>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_date"><img src="images/icon_date.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Срок хранения</span>
			        	<span class="tableText-blue">12 суток</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_weght"><img src="images/icon_weght.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Вес</span>
			        	<span class="tableText-blue">114 грамм</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_list"><img src="images/icon_list.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Статьи</span>
			        	<span class="tableText-blue">+5</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_food_lit"><img src="images/icon_food_lit.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Рецепты</span>
			        	<span class="tableText-blue">+3</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableBottom">
		        	<div class="tableBottom-content">
		        		<div class="tableBottom-content-left">
		        			<div class="sprite sprite-icon_bdate">
		        				<span class="bdate-number">28</span>
		        				<span class="bdate-month">Авг</span>
		        			</div>
		        		</div>
		        		<div class="tableBottom-content-right">
		        			<p>Ближайшая доставка</p>
		        			<p><span>28</span> августа <span>06:00 - 09:00</span></p>
		        		</div>
		        	</div>
		        </td>
		    </tr>
		</table>
	</div>
</div>