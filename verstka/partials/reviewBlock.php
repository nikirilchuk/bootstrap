<div class="reviewBlock">
	<div class="reviewBlock-item">
		<div class="reviewBlock-item-name">
			Андрей
		</div>
		<div class="reviewBlock-item-content">
			No est soleat inimicus abhorreant, mea primis nusquam omnesque an. Vim in tibique conceptam. Ius id discere postulant facilisis, ex eam simul debitis. Te ferri fierent delicatissimi mei.
		</div>
	</div>
	<div class="reviewBlock-item">
		<div class="reviewBlock-item-name">
			Максим
		</div>
		<div class="reviewBlock-item-content">
			Ad his congue sententiae, legere putent mediocritatem eu quo. Ne simul voluptua reformidans has,te viris mollis scribentur sed, duo cu illud simul veritus. Cum id quidam facilis dissentiet, vix ea brute nominati. Choro nonumes at nam, eum eu sale ullamcorper, viris dolorem mea ei. Vitae nostrud mel in. Ea illud nonumy appareat ius.
		</div>
	</div>
	<div class="reviewBlock-item">
		<div class="reviewBlock-item-name">
			Роман
		</div>
		<div class="reviewBlock-item-content">
			No est soleat inimicus abhorreant, mea primis nusquam omnesque an. Vim in tibique conceptam. Ius id discere postulant facilisis, ex eam simul debitis. Te ferri fierent delicatissimi mei.
		</div>
	</div>
	<a href="#" class="btn btn-primary btn-md reviewBlock-button ">Напиcать отзыв</a>
	<div class="reviewBlock-form">
		<div class="zForm zNice jsForm">
			<div class="zForm-title">
				Напишите свой КОММЕНТАРИЙ
			</div>
			<form action="ajsx.php">
				<div class="completePopup-text" data-title="Комментарий оставлен" data-text="Опубликуется после обработки модератором"></div>
				<div class="zForm-row">
					<div class="zForm-col">
						<input type="text" name="text1" required="required" placeholder="Имя" />
					</div>
					<div class="zForm-col">
						<input type="email" name="text2" required="required" placeholder="E-Mail" />
					</div>
				</div>
				<div class="zForm-row">
					<textarea name="text3" rows="5" placeholder="Ваш комментарий" required="required"></textarea>
				</div>
				<div class="zForm-row">
					<input type="submit" class="btn btn-primary btn-md" value="Написать отзыв"/>
				</div>
			</form>
		</div>
	</div>
</div>