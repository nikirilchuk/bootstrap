<div class="zHiddenBlock">
	<div id="formMessage" class="popupForm">
		<div class="formMessage">
			<div class="zForm-title">
				ваша заявка отправлена!
			</div>
			<div class="zForm-subtitle">
				Ваша заявка. Менеджер ответит Вам в течение 3-х рабочих дней.
			</div>
		</div>
	</div>
	<div id="loginForm" class="popupForm popupWrap">
		<div class="formMessage">
			<div class="zForm-title">Вход</div>
			<div class="zForm-inner zNice">
				<form action="ajax.php">
					<div class="zForm-row">
						<input type="email" name="email" placeholder="E-Mail" required="required"/>
					</div>
					<div class="zForm-row">
						<input type="password" name="pass" placeholder="Пароль" required="required"/>
					</div>
					<div class="zForm-row">
						<div class="zForm-col text-left">
							<input type="submit" class="btn-primary btn-md" value="Войти" />
						</div>
						<div class="zForm-col text-left">
							<div class="zForm-link">
								<a href="#forgetForm" class="fancybox-popup">Забыли пароль?</a>
							</div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="registerForm" class="popupForm popupWrap">
		<div class="formMessage">
			<div class="zForm-title">Регистрация</div>
			<div class="zForm-inner zNice">
				<form action="ajax.php">
					<div class="zForm-row">
						<div class="checkout-col">
							<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
							<div class="zForm-inner mlForm">
								<div class="zForm-row">
									<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="text" name="text2" placeholder="Фамилия" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="email" name="email" placeholder="E-Mail" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
								</div>
							</div>
							<div class="zForm-title">ВАШ ПАРОЛЬ</div>
							<div class="zForm-inner mlForm passwordForm">
								<div class="zForm-row">
									<input type="password" name="text1" id="passwordo" placeholder="* Пароль:" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="password" name="text2" id="re-passwordo" placeholder="* Повторите пароль:" required="required"/>
								</div>
							</div>
							<div class="zForm-title">РАССЫЛКА НОВОСТЕЙ</div>
							<div class="zForm-inner mlForm checkboxsesForm">
								<div class="zForm-subtitle">Подписка на новости:</div>
								<div class="zForm-row">
									<div class="zForm-col">
										<label>
											<input type="radio" name="checradio"  checked="checked"/> <span class="zForm-text">Да </span>
										</label>
									</div>
									<div class="zForm-col">
										<label>
											<input type="radio" name="checradio" /> <span class="zForm-text">Нет  </span>
										</label>
									</div>
								</div>
							</div>
						</div>
						<div class="checkout-col">
							<div class="zForm-title">Адрес</div>
							<div class="zForm-inner mlForm">
								<div class="zForm-row">
									<input type="text" name="address" placeholder="Регион / Область:" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="text" name="address2" placeholder="Адрес" required="required"/>
								</div>
							</div>
						</div>
					</div>
					<div class="zForm-row text-right buttonRow">
						<input type="submit" class="btn-primary btn-md" value="Зарегистрироваться" />
					</div>
				</form>
			</div>
		</div>
	</div><div id="registerForm_m" class="popupForm popupWrap">
		<div class="formMessage">
			<div class="zForm-title">Регистрация</div>
			<div class="zForm-inner zNice">
				<form action="ajax.php">
					<div class="zForm-row">
						<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
						<div class="zForm-inner">
							<div class="zForm-row">
								<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
							</div>
							<div class="zForm-row">
								<input type="text" name="text2" placeholder="Фамилия" required="required"/>
							</div>
							<div class="zForm-row">
								<input type="email" name="email" placeholder="E-Mail" required="required"/>
							</div>
							<div class="zForm-row">
								<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
							</div>
						</div>
						<div class="zForm-title">Адрес</div>
						<div class="zForm-inner">
							<div class="zForm-row">
								<input type="text" name="address" placeholder="Регион / Область:" required="required"/>
							</div>
							<div class="zForm-row">
								<input type="text" name="address2" placeholder="Адрес" required="required"/>
							</div>
						</div>
						<div class="zForm-title">ВАШ ПАРОЛЬ</div>
						<div class="zForm-inner passwordForm">
							<div class="zForm-row">
								<input type="password" name="pass1" id="passwordo" placeholder="* Пароль:" required="required"/>
							</div>
							<div class="zForm-row">
								<input type="password" name="pass2" id="re-passwordo" placeholder="* Повторите пароль:" required="required"/>
							</div>
						</div>
						<div class="zForm-title">РАССЫЛКА НОВОСТЕЙ</div>
						<div class="zForm-subtitle">Подписка на новости:</div>
						<div class="zForm-row text-center checkboxsesForm">
							<label>
								<input type="radio" name="checradio"  checked="checked"/> <span class="zForm-text">Да </span>
							</label>

							<label>
								<input type="radio" name="checradio" /> <span class="zForm-text">Нет  </span>
							</label>
						</div>
					</div>
					<div class="zForm-row text-center">
						<input type="submit" class="btn-primary btn-lg" value="Зарегистрироваться" />
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="forgetForm" class="popupForm popupWrap">
		<div class="formMessage">
			<div class="zForm-title">Востановление пароля</div>
			<div class="zForm-inner zNice">
				<form action="ajax.php">
					<div class="zForm-row">
						<input type="email" name="email" placeholder="E-Mail" required="required"/>
					</div>
					<div class="zForm-row">
						<input type="submit" class="btn-primary btn-md" value="Войти" />
					</div>
				</form>
			</div>
		</div>
	</div>
	<div id="recallForm" class="popupForm popupWrap jsForm zNice">
		<form action="ajax.php">
			<div class="formMessage">
				<div class="completePopup-text" data-title="Ожидайте" data-text="Мы свяжемся с вами в ближайшее время"></div>
				<div class="zForm-title">Заказать звонок</div>
				<div class="zForm-inner">
					<div class="zForm-row">
						<input type="text" name="name" placeholder="Имя" />
					</div>
					<div class="zForm-row">
						<input type="text" name="phone" class="phoneMask" placeholder="Телефон" required="required"/>
					</div>
					<div class="zForm-row">
						<div class="text-left">
							<input type="submit" class="btn-primary btn-md" value="Заказать" />
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<div id="videoPopup" class="popupForm popupWrap">
		<div class="formMessage">
			<div class="videoIframe"></div>
		</div>
	</div>
</div>