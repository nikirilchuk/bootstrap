<?php
if (@is_file('config.php')) include_once('config.php');

if ($zEnv=='dev'):?>
	<script src="js/jquery.min.js" type="text/javascript"></script>
	<script src="js/jquery.browser.min.js" type="text/javascript"></script>

	<script src="js/selectivizr-min.js" type="text/javascript"></script>
	<script src="js/bootstrap/bootstrap.min.js" type="text/javascript"></script>

	<script src="js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js/jquery.znice.validate.js" type="text/javascript"></script>
	<script src="js/jquery.znice.js" type="text/javascript"></script>

	<script src="fancybox/jquery.fancybox.js" type="text/javascript"></script>

	<script src="js/modernizr.js" type="text/javascript"></script>
	<script src="js/jquery.watermark.min.js" type="text/javascript"></script>
	<script src="js/maskedinput.js" type="text/javascript"></script>
	<script src="js/slick.js" type="text/javascript"></script>
	<script src="js/flipclock.min.js" type="text/javascript"></script>

	<script src="develop/john.js" type="text/javascript"></script>

	<script src="js/myalert.js" type="text/javascript"></script>
	<script src="develop/autocssrenew.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="css/undblock.css" media="all" />
	<script src="js/scr.js" type="text/javascript"></script>
<?php else:?>
	<script type="text/javascript" src="js/general.js"></script>
<?php endif;?>