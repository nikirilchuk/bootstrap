<div class="receptItem">
	<div class="newsBlock-item">
		<div class="newsBlock-title">
			Доставка в любой день Недели!
		</div>
		<div class="newsBlock-item-data">
			30.09.2013
		</div>
		<div class="newsBlock-item-text">
			Друзья! Мы рады сообщить вам, что с 31 августа мы будем доставлять ваши заказы всю неделю, в любой день, включая воскресенье<br />  и понедельник. 
		</div>
		<div class="newsBlock-item-all">
			<a href="#">Читать далее...</a>
		</div>
	</div>
</div>