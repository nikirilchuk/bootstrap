<div class="productsFilter zForm zNice">
	<form action="ajax.php">
		<div class="zForm-row">
			<div class="zForm-row mbnone">
				<div class="zForm-title ">
					Цена упаковки
				</div>
			</div>
			<div class="zForm-row">
				<div class="zForm-col">
					<div class="zForm-lbl">
						от
					</div>
					<div class="zForm-input">
						<input type="text" name="n1"/>
					</div>
				</div>
				<div class="zForm-col">
					<div class="zForm-lbl">
						до
					</div>
					<div class="zForm-input">
						<input type="text" name="n1"/>
					</div>
				</div>
			</div>
			<div class="zForm-row mbnone">
				<div class="zForm-title">
					Калорийность
				</div>
				<div class="zForm-col">
					<div class="zForm-lbl">
						от
					</div>
					<div class="zForm-input">
						<input type="text" name="n1"/>
					</div>
				</div>
				<div class="zForm-col">
					<div class="zForm-lbl">
						до
					</div>
					<div class="zForm-input">
						<input type="text" name="n1"/>
					</div>
				</div>
			</div>
			<div class="zForm-row">&nbsp;</div>
			<div class="zForm-row">
				<select>
					<option>Способ обработки</option>
					<option>Способ обработк1и</option>
					<option>Способ обработки2</option>
					<option>Способ обработки3</option>
					<option>Способ обработки4</option>
				</select>
			</div>
			<div class="zForm-row">
				<select>
					<option>Вид фасовки</option>
					<option>Вид фасовки</option>
					<option>Вид фасовки</option>
					<option>Вид фасовки</option>
					<option>Вид фасовки</option>
				</select>
			</div>
			<div class="zForm-row">
				<select>
					<option>Место Улова</option>
					<option>Место Улова</option>
					<option>Место Улова</option>
					<option>Место Улова</option>
					<option>Место Улова</option>
					<option>Место Улова</option>
				</select>
			</div>
			<div class="zForm-row">
				<select>
					<option>Готовность к употреблению</option>
					<option>Готовность к употреблению</option>
					<option>Готовность к употреблению</option>
					<option>Готовность к употреблению</option>
				</select>
			</div>
			<div class="zForm-row">
				<select>
					<option>Производитель</option>
					<option>Производитель</option>
					<option>Производитель</option>
					<option>Производитель</option>
					<option>Производитель</option>
				</select>
			</div>
			<div class="zForm-row">
				<select>
					<option>Содержание соли</option>
					<option>Содержание соли</option>
					<option>Содержание соли</option>
					<option>Содержание соли</option>
					<option>Содержание соли</option>
					<option>Содержание соли</option>
				</select>
			</div>
		</div>
		<div class="zForm-row text-center productsFilter-checkbox">
			<label>
				<input type="checkbox" name="check" />
				<span class="zForm-text">Спецпредложение</span>
			</label>
		</div>
		<div class="zForm-row text-center">
			<input type="submit" value="Показать" class="btn btn-default btn-lg"/>
			<input type="reset" value="Сбросить" class="btn btn-primary btn-lg"/>
		</div>
	</form>
</div>