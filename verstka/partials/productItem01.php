<div class="productItem">
	<div class="productItem-top">
		<a href="#">
			<div class="icon-label-hit">
				<img src="images/lable-hit.png" alt="" />
			</div>
			<div class="productItem-top-imag">
				<img src="images/product-item.png" alt="" /><span class="vfix"></span>
			</div>
			<div class="productItem-top-text">
				КРАСНАЯ ИКРА КЕТЫ<br/> "АЛЫЙ ЖЕМЧУГ" СТ\Б, 500Г.
			</div>
		</a>
	</div>
	<div class="productItem-bottom">
		<div class="productItem-bottom-price">
			657 p.
		</div>
		<div class="productItem-bottom-button">
			<a href="#" class="btn btn-default"><span class="icon sprite-icon_ko"><img src="images/icon_ko.png" alt=""></span><span class="icon sprite-icon_ko"><img src="images/icon-ko-hover.png" alt=""></span>В КОРЗИНУ</a>
			<a href="#" class="btn btn-primary">КУПИТЬ В 1 КЛИК</a>
		</div>
	</div>
</div>