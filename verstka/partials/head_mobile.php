<?php
if (@is_file('config.php')) include_once('config.php');

if ($zEnv=='dev'):?>
	<script src="js_m/jquery.min.js" type="text/javascript"></script>
	<script src="js_m/jquery.browser.min.js" type="text/javascript"></script>
	
	<script src="js_m/bootstrap/bootstrap.min.js" type="text/javascript"></script>

	<script src="js_m/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js_m/jquery.znice.validate.js" type="text/javascript"></script>
	<script src="js_m/jquery.znice.js" type="text/javascript"></script>

	<script src="fancybox/jquery.fancybox.js" type="text/javascript"></script>

	<script src="js_m/maskedinput.js" type="text/javascript"></script>
	<script src="js_m/slick.js" type="text/javascript"></script>
	<script src="js_m/jquery.mobile.custom.min.js" type="text/javascript"></script>


	<script src="js_m/myalert.js" type="text/javascript"></script>
	<script src="develop/autocssrenew.js" type="text/javascript"></script>
	<link rel="stylesheet" type="text/css" href="css_m/undblock.css" media="all" />
	<script src="js_m/scr.js" type="text/javascript"></script>
<?php else:?>
	<script type="text/javascript" src="js_m/general.js"></script>
<?php endif;?>