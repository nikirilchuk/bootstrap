<div class="productGallery">
	<div class="productGallery-top">
		
		<div class="slider slider-for">
			<div>
				<div class="productGallery-item">
					<div class="productGallery-item-top">
						<img src="images/news-item.jpg" alt="" /><div class="vfix"></div>
					</div>
				</div>
			</div>
			<div>
				<div class="productGallery-item">
					<div class="productGallery-item-top">
						<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
					</div>
				</div>
			</div>
			<div>
				<div class="productGallery-item">
					<div class="productGallery-item-top">
						<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
					</div>
				</div>
			</div>
			<div>
				<div class="productGallery-item">
					<div class="productGallery-item-top">
						<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
					</div>
				</div>
			</div>
			<div>
				<div class="productGallery-item">
					<div class="productGallery-item-top">
						<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="productGallery-item-bottom">
			<a href="#">
				<span class="icon icon-star active"></span>
			</a>
			<a href="#">
				<span class="icon icon-star active"></span>
			</a>
			<a href="#">
				<span class="icon icon-star active"></span>
			</a>
			<a href="#">
				<span class="icon icon-star"></span>
			</a>
			<a href="#">
				<span class="icon icon-star"></span>
			</a>
		</div>
	</div>
	<div class="slider slider-nav">
		<div>
			<div class="productGallery-imag">
				<img src="images/news-item.jpg" alt="" /><div class="vfix"></div>
			</div>
		</div>
		<div>
			<div class="productGallery-imag">
				<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
			</div>
		</div>
		<div>
			<div class="productGallery-imag">
				<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
			</div>
		</div>
		<div>
			<div class="productGallery-imag">
				<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
			</div>
		</div>
		<div>
			<div class="productGallery-imag">
				<img src="images/pro-gallery01.jpg" alt="" /><div class="vfix"></div>
			</div>
		</div>
	</div>
	<div class="product-video">
		<a href="#" class="fancybox-video" data-video="ClVcPyEfcF0">
			<span class="icon sprite-icon_camera_blue"><img src="images/icon_cam_lit.png" alt=""/></span>
			<span>Видео о товаре</span>
		</a>
	</div>
	<div class="productSoc">
		<a href="#"><span class="icon sprite-icon_tv_blue"><img src="images/fsoc1-blue.png" alt=""/></span></a>
		<a href="#"><span class="icon sprite-icon_vk_blue"><img src="images/fsoc2-blue.png" alt=""/></span></a>
		<a href="#"><span class="icon sprite-icon_fb_blue"><img src="images/fsoc3-blue.png" alt=""/></span></a>
		<a href="#"><span class="icon sprite-icon_gp_blue"><img src="images/fsoc4-blue.png" alt=""/></span></a>
		<a href="#"><span class="icon sprite-icon_od_blue"><img src="images/fsoc5-blue.png" alt=""/></span></a>
		<a href="#"><span class="icon sprite-icon_inst_blue"><img src="images/fsoc6-blue.png" alt=""/></span></a>
	</div>
</div>