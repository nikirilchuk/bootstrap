<div class="productsFilter zForm zNice">
	<form action="ajax.php">
		<div class="zForm-row mbnone">
			<div class="productsFilter-left">
				<div class="zForm-row">
					<div class="zForm-title">
						Цена упаковки
					</div>
				</div>
				<div class="zForm-row">
					<div class="zForm-col">
						<div class="zForm-lbl">
							от
						</div>
						<div class="zForm-input">
							<input type="text" name="n1"/>
						</div>
					</div>
					<div class="zForm-col">
						<div class="zForm-lbl">
							до
						</div>
						<div class="zForm-input">
							<input type="text" name="n1"/>
						</div>
					</div>
				</div>
				<div class="zForm-row mbnone">
					<div class="zForm-title">
						Калорийность
					</div>
					<div class="zForm-col">
						<div class="zForm-lbl">
							от
						</div>
						<div class="zForm-input">
							<input type="text" name="n1"/>
						</div>
					</div>
					<div class="zForm-col">
						<div class="zForm-lbl">
							до
						</div>
						<div class="zForm-input">
							<input type="text" name="n1"/>
						</div>
					</div>
				</div>
			</div>
			<div class="productsFilter-right">
				<div class="zForm-row">&nbsp;</div>
				<div class="zForm-row zForm-treeCol">
					<div class="zForm-col">
						<select>
							<option>Способ обработки</option>
							<option>Способ обработк1и</option>
							<option>Способ обработки2</option>
							<option>Способ обработки3</option>
							<option>Способ обработки4</option>
						</select>
					</div>
					<div class="zForm-col">
						<select>
							<option>Вид фасовки</option>
							<option>Вид фасовки</option>
							<option>Вид фасовки</option>
							<option>Вид фасовки</option>
							<option>Вид фасовки</option>
						</select>
					</div>
					<div class="zForm-col">
						<select>
							<option>Место Улова</option>
							<option>Место Улова</option>
							<option>Место Улова</option>
							<option>Место Улова</option>
							<option>Место Улова</option>
							<option>Место Улова</option>
						</select>
					</div>
				</div>
				<div class="zForm-row mbnone zForm-treeCol">
					<div class="zForm-col">
						<select>
							<option>Готовность к употреблению</option>
							<option>Готовность к употреблению</option>
							<option>Готовность к употреблению</option>
							<option>Готовность к употреблению</option>
						</select>
					</div>
					<div class="zForm-col">
						<select>
							<option>Производитель</option>
							<option>Производитель</option>
							<option>Производитель</option>
							<option>Производитель</option>
							<option>Производитель</option>
						</select>
					</div>
					<div class="zForm-col">
						<select>
							<option>Содержание соли</option>
							<option>Содержание соли</option>
							<option>Содержание соли</option>
							<option>Содержание соли</option>
							<option>Содержание соли</option>
							<option>Содержание соли</option>
						</select>
					</div>
				</div>
			</div>
		</div>
		<div class="zForm-row text-right mbnone leftFormat">
			<div class="zForm-col">
				<label>
					<input type="checkbox" name="check" />
					<span class="zForm-text">Спецпредложение</span>
				</label>
			</div>
			<div class="zForm-col">
				<input type="submit" value="Показать" class="btn btn-default btn-sm"/>
				<input type="reset" value="Сбросить" class="btn btn-primary btn-sm"/>
			</div>
		</div>
	</form>
</div>