<div class="receptItem">
	<div class="newsBlock-item">
		<div class="newsBlock-title">
			Запеченная в духовке сайра в горшочках
		</div>
		<div class="receptItem-time">
			<div class="receptItem-time-left">
				<span class="icon icon-time"><img src="images/icon-time.png" alt=""/></span>30 мин.
			</div>
			<div class="receptItem-time-right">
				<span class="icon icon-time-right"><img src="images/icon-time-right.png" alt=""/></span>Несложный
			</div>
		</div>
		<div class="newsBlock-item-text">
			Вкусный рецепт запеченной сайры порадует вас быстротой и простотой приготовления. Блюдо в меру сытное, подойдет как для завтрака, так и для ужина, а ароматный гарнир приготовленный из морковного маринада приятно оттенит вкусовые оттенки рыбы.
		</div>
		<div class="newsBlock-item-all">
			<a href="#">Подробнее</a>
		</div>
	</div>
</div>