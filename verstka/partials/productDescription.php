<div class="productDescription">
	<div class="productDescription-title">
		<h2>
			КРАСНАЯ ИКРА КЕТЫ "ИТА СЕВЕРНАЯ КОМПАНИЯ" СТ\Б, 114г.
		</h2>
	</div>
	<div class="zForm zNice productPrice">
		<div class="basketForm-col">
			<div class="basketProdItem-price"><span>650</span> p.</div>
		</div>
		<div class="basketForm-col">
			<div class="basketProdItem-count">
				<input type="text" data-ztype="qInput" class="leftButton rightButton" value="1" data-price="650" /><span> шт.</span>
			</div>
		</div>
	</div>
	<div class="productDescription-buttons">
		<a href="#" class="btn btn-primary">Заказать</a>
		<a href="#" class="btn btn-success">В один клик</a>
	</div>
	<div class="productDescription-table">
		<table>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_cel"><img src="images/icon_cel.png" alt=""></span>
			        	</div>
			        	<div class="tableText-center">
			        		Температура хранения
			        	</div>
			        	<div class="tableText-blue">
			        		0-4 с
			        	</div>
		        	</div>
		        </td>
		        <td class="tableTop-right" rowspan="5">
		        	<div class="timer-wrap">
		        		<div class="timer-title">
		        			Лучшее ценовое предложение!
		        		</div>
		        		<script>
		        			var now = new Date();
								year = 2015; month = 10; day = 20 ;hour= 0; min= 0; sec= 0; 
		        		</script>
		        		
		        		<div id="countbox" class="timerBlock">
		        			<div class="timerBlock-subtitle">
			        			До конца акции осталось:
			        		</div>
		        			<div id="days" class="timerBlock-col">
		        				<span></span>37
		        				<div id="days_text">дни</div>
		        			</div>
		        			<div id="hours" class="timerBlock-col">
		        				<span></span>9
		        				<div id="hours_text">час</div>
		        			</div>
		        			<div id="mins" class="timerBlock-col">
		        				<span></span>47
		        				<div id="mins_text">мин</div>
		        			</div>
		        			<div id="secs" class="timerBlock-col">
		        				<span></span>42
		        				<div id="secs_text">сек</div>
		        			</div>
		        			<div class="clear"></div>
		        		</div>
		        		<div class="timerBlock-link text-center">
		        			<a href="#">Подробнее о акции</a>
		        		</div>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_date"><img src="images/icon_date.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Срок хранения</span>
			        	<span class="tableText-blue">12 суток</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_weght"><img src="images/icon_weght.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Вес</span>
			        	<span class="tableText-blue">114 грамм</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_list"><img src="images/icon_list.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Статьи</span>
			        	<span class="tableText-blue">+5</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableTop-left">
		        	<div class="tableLeft-content">
			        	<div class="tableText-iconLeft">
			        		<span class="icon sprite-icon_food_lit"><img src="images/icon_food_lit.png" alt=""></span>
			        	</div>
			        	<span class="tableText-center">Рецепты</span>
			        	<span class="tableText-blue">+3</span>
		        	</div>
		        </td>
		    </tr>
		    <tr>
		        <td class="tableBottom" colspan="2">
		        	<div class="tableBottom-content">
		        		<div class="tableBottom-content-left">
		        			<div class="sprite sprite-icon_bdate">
		        				<span class="bdate-number">28</span>
		        				<span class="bdate-month">Авг</span>
		        			</div>
		        		</div>
		        		<div class="tableBottom-content-right">
		        			<p>Ближайшая доставка</p>
		        			<p><span>28</span> августа <span>06:00 - 09:00</span></p>
		        		</div>
		        	</div>
		        </td>
		    </tr>
		</table>
	</div>
</div>