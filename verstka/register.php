	<?include 'header.php';?>
	<div class="registerPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							РЕГИСТРАЦИЯ
						</h3>
						<div class="zForm checkout checkoutStep-one">
							<div class="zForm-head">
								Войти / Регистрация
							</div>
							<div class="zForm-row">
								<div class="checkout-col startForm">
									<div class="zForm-title">РЕГИСТРАЦИЯ</div>
									<div class="zForm-subtitle">Создав учетную запись Вы сможетете быстрее оформлять заказы, отслеживать их статус и просматривать историю покупок.</div>
									<div class="zForm-inner">
										<div class="zForm-row">
										</div>
										<div class="zForm-row">
											<a href="#" class="btn btn-primary btn-md">Продолжить</a>
										</div>
									</div>
								</div>
								<div class="checkout-col">
									<div class="zForm-title">ЗАРЕГИСТРИРОВАННЫЙ ПОЛЬЗОВАТЕЛЬ</div>
									<div class="zForm-subtitle">Я совершал здесь покупки ранее и регистрировался</div>
									<div class="zForm-inner zNice">
										<form action="ajax.php">
											<div class="zForm-row">
												<input type="email" name="email" placeholder="E-Mail" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="password" name="pass" placeholder="Пароль" required="required"/>
											</div>
											<div class="zForm-row">
												<div class="zForm-link">
													<a href="#">Забыли пароль?</a>
												</div>
												<input type="submit" class="btn-primary btn-md" value="Войти" />
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>