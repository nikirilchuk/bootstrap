<?include 'header_mobile.php';?>
<div class="checkoutPage innerPage">
	<div class="container main">
		<div class="row">
			<div class="innerContent">
				<h3 class="text-center">
					ОФОРМЛЕНИЕ ЗАКАЗА
				</h3>
				<div class="zForm checkout checkoutStep-two zNice">
					<form action="ajax.php">
						<div class="zForm-row">
							<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
							<div class="zForm-inner">
								<div class="zForm-row">
									<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="text" name="text2" placeholder="Фамилия" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="email" name="email" placeholder="E-Mail" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
								</div>
							</div>
							<div class="zForm-title">Адрес</div>
							<div class="zForm-inner">
								<div class="zForm-row">
									<select>
										<option value="0">Московская область</option>
										<option value="1">Московская область</option>
										<option value="2">Московская область</option>
										<option value="3">Московская область</option>
										<option value="4">Московская область</option>
										<option value="5">Московская область</option>
									</select>
								</div>
								<div class="zForm-row">
									<select>
										<option value="0">Москова</option>
										<option value="1">Москова</option>
										<option value="2">Москова</option>
										<option value="3">Москова</option>
										<option value="4">Москова</option>
										<option value="5">Москова</option>
									</select>
								</div>
								<div class="zForm-row">
									<input type="text" name="address" placeholder="Адрес" required="required"/>
								</div>
							</div>
							<div class="zForm-title">Cпособ оплаты</div>
							<div class="zForm-inner payForm">
								<div class="zForm-row">
									<label><input type="radio" name="checradio" checked="checked"/> <span class="zForm-text">Оплата при доставке </span></label>
								</div>
								<div class="zForm-row">
									<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images_m/pay_img1.png" alt=""/> </span></label>
								</div>
								<div class="zForm-row">
									<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images_m/pay_img2.png" alt=""/>  </span></label>
								</div>
								<div class="zForm-row">
									<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images_m/pay_img3.png" alt=""/>  </span></label>
								</div>
							</div>
							<div class="zForm-row">
								<textarea rows="4" name="text-2" required="required" placeholder="Ваш комментарий:"></textarea>
							</div>
							<div class="zForm-row text-center">
								<input type="submit" class="btn-primary btn-lg" value="Подтверждение заказа" />
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?include 'footer_mobile.php';?>