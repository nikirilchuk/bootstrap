<?include 'header.php';?>
<div class="basketPage innerPage">
	<div class="container main">
		<div class="row">
			<?partial('leftMenu');?>
			<div class="content_w">
				<div class="content">
					<?partial('breadcrumbs');?>
					<h3 class="text-center">
						КОРЗИНА ПОКУПОК
					</h3>
					<div class="zForm checkout checkoutStep-four zNice">
						<form action="ajax.php">
							<div class="completePopup-text" data-title="Заказ выполнен" data-text="Заказ будет обработан в ближайшее время"></div>
							<div class="basketForm">
								<div class="basketForm-head">
									<div class="basketForm-col">
										Фото
									</div>
									<div class="basketForm-col">
										Наименование товара
									</div>
									<div class="basketForm-col">
										Кол-во
									</div>
									<div class="basketForm-col">
										Цена
									</div>
									<div class="basketForm-col">
										Итого
									</div>
								</div>
								<div class="basketForm-body">
									<div class="basketForm-col">
										<div class="basketProdItem-img"><a href="#"><img src="images/productimg.jpg" alt=""/></a></div>
									</div>
									<div class="basketForm-col">
										<a href="#" class="basketProdItem-title">КРАСНАЯ ИКРА КЕТЫ<br/>  "ИТА СЕВЕРНАЯ КОМПАНИЯ"<br/> СТ\Б, 114Г.</a>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-count">
											<input type="text" data-ztype="qInput" class="leftButtons" value="1" data-price="650" />
										</div>
										<a href="#" class="basketForm-remove icon"><img src="images/bsket_remove.png" alt=""/></a>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-price"><span>650</span> p.</div>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-price rowPrice" data-rowprice="650"><span>650</span> p.</div>
									</div>
								</div>
								<div class="basketForm-body">
									<div class="basketForm-col">
										<div class="basketProdItem-img"><a href="#"><img src="images/productimg.jpg" alt=""/></a></div>
									</div>
									<div class="basketForm-col">
										<a href="#" class="basketProdItem-title">КРАСНАЯ ИКРА КЕТЫ<br/>  "ИТА СЕВЕРНАЯ КОМПАНИЯ"<br/> СТ\Б, 114Г.</a>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-count">
											<input type="text" data-ztype="qInput" class="leftButtons" value="1" data-price="650"/>
										</div>
										<a href="#" class="basketForm-remove icon"><img src="images/bsket_remove.png" alt=""/></a>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-price"><span>650</span> p.</div>
									</div>
									<div class="basketForm-col">
										<div class="basketProdItem-price rowPrice" data-rowprice="650"><span>650</span> p.</div>
									</div>
								</div>
							</div>
							<div class="basketForm-bottom">
								<div class="basketForm-bottom-left">
									<div class="zForm-title">
										ВОСПОЛЬЗУЙТЕСЬ ДОПОЛНИТЕЛЬНЫМИ ВОЗМОЖНОСТЯМИ
									</div>
									<div class="zForm-subtitle mlForm">
										Если у вас есть код купона на скидку или бонусные баллы, которые вы хотите использовать, выберите соответствующий пункт. 
									</div>
									<div class="zForm-inner mlForm basketDiscont-form">
										<div class="zForm-row">
											<label>
												<input type="radio" name="text1" checked="checked" class="cuponeDiscont"/>  <span class="zForm-text">Использовать купон на скидку </span>
											</label>
										</div>
										<div class="zForm-row dibi">
											<input type="text" class="numberInput redColor" value="3213-2132-1352-1321"/> <div class="basketForm-discont" data-discontcart="10" data-discontsert="500">У Вас <span>10%</span> скидки!</div>
										</div>
										<div class="zForm-row">
											<label>
												<input type="radio" name="text1"/>  <span class="zForm-text">Использовать подарочный сертификат </span>
											</label>
										</div>
									</div>
								</div>
								<div class="basketForm-bottom-right">
									<div class="basketAllprice text-right">
										<div class="basketAllprice-row">
											Сумма <span class="basketAllprice-sum"><span>1300</span>р</span>
										</div>
										<div class="basketAllprice-row redColor">
											Скидка <span class="basketAllprice-discont" ><span>130</span>р</span>
										</div>
										<div class="basketAllprice-row">
											Итого <span class="basketAllprice-basket"><span>1170</span>р</span>
										</div>
									</div>
								</div>
							</div>
							<div class="zForm-row text-right buttonRow">
								<input type="submit" class="btn-default btn-md" value="ПРОДОЛЖИТЬ ПОКУПКИ" />
								<input type="submit" class="btn-primary btn-md" value="Подтверждение заказа" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?include 'footer.php';?>