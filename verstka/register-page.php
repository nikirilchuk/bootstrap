	<?include 'header.php';?>
	<div class="registerPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							РЕГИСТРАЦИЯ
						</h3>
						<div class="zForm checkout checkoutStep-two zNice">
							<div class="zForm-head">
								Ререгистрация
							</div>
							<form action="ajax.php">
								<div class="zForm-row">
									<div class="checkout-col">
										<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
										<div class="zForm-inner mlForm">
											<div class="zForm-row">
												<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="text" name="text2" placeholder="Фамилия" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="email" name="email" placeholder="E-Mail" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
											</div>
										</div>
										<div class="zForm-title">ВАШ ПАРОЛЬ</div>
										<div class="zForm-inner mlForm passwordForm">
											<div class="zForm-row">
												<input type="password" name="text1" id="passwordo" placeholder="* Пароль:" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="password" name="text2" id="re-passwordo" placeholder="* Повторите пароль:" required="required"/>
											</div>
										</div>
										<div class="zForm-title">РАССЫЛКА НОВОСТЕЙ</div>
										<div class="zForm-inner mlForm checkboxsesForm">
											<div class="zForm-subtitle">Подписка на новости:</div>
											<div class="zForm-row">
												<div class="zForm-col">
													<label>
														<input type="radio" name="checradio"  checked="checked"/> <span class="zForm-text">Да </span>
													</label>
												</div>
												<div class="zForm-col">
													<label>
														<input type="radio" name="checradio" /> <span class="zForm-text">Нет  </span>
													</label>
												</div>
											</div>
										</div>
									</div>
									<div class="checkout-col">
										<div class="zForm-title">Адрес</div>
										<div class="zForm-inner mlForm">
											<div class="zForm-row">
												<input type="text" name="address" placeholder="Регион / Область:" required="required"/>
											</div>
											<div class="zForm-row">
												<input type="text" name="address2" placeholder="Адрес" required="required"/>
											</div>
										</div>
									</div>
								</div>
								<div class="zForm-row text-right buttonRow">
									<input type="submit" class="btn-primary btn-md" value="Зарегистрироваться" />
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>