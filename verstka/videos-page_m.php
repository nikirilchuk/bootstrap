	<?include 'header_mobile.php';?>
	<div class="videosPage innerPage">
		<div class="container main">
			<div class="row">
				<div class="content">
					<?partial('breadcrumbs');?>
					<h3 class="text-center">
						Видео
					</h3>
					<div class="videosPage-items">
						<div class="mgBlock">
							<div class="thirdheading">
								<span>РЕЦЕПТЫ</span>
							</div>
							<?partial('video-items_m');?>
							<div class="text-center">
								<a href="#" class="slink">Все видео</a>
							</div>
						</div>
						<div class="mgBlock">
							<div class="thirdheading">
								<span>ОТЗЫВЫ</span>
							</div>
							<?partial('video-items_m');?>
							<div class="text-center">
								<a href="#" class="slink">Все видео</a>
							</div>
						</div>
						<div class="mgBlock">
							<div class="thirdheading">
								<span>ТОВАРЫ</span>
							</div>
							<?partial('video-items_m');?>
							<div class="text-center">
								<a href="#" class="slink">Все видео</a>
							</div>
						</div>
						<div class="mgBlock">
							<div class="thirdheading">
								<span>КОНКУРСЫ</span>
							</div>
							<?partial('video-items_m');?>
							<div class="text-center">
								<a href="#" class="slink">Все видео</a>
							</div>
						</div>
						<div class="mgBlock">
							<div class="thirdheading">
								<span>СРАВНЕНИЕ ПРОДУКЦИИ</span>
							</div>
							<?partial('video-items_m');?>
							<div class="text-center">
								<a href="#" class="slink">Все видео</a>
							</div>
						</div>
					</div>
					<?partial('paginator');?>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>