<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="" />
	<meta name="keywords" content="" />
	<meta name="viewport" content="width=device-width, initial-scale=1"/>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/style.css" media="all" />
	<link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,700&amp;subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=Exo+2&amp;subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
		<script>
		document.createElement('header');
		document.createElement('nav');
		document.createElement('section');
		document.createElement('article');
		document.createElement('aside');
		document.createElement('footer');
		document.createElement('time');
		</script>	
		<script src="js/pie.js" type="text/javascript"></script>
		<script src="http://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      	<script src="http://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<![endif]-->

	<!--[if lt IE 8]><script src="js/oldie/warning.js"></script><script>window.onload=function(){e("js/oldie/")}</script><![endif]-->
</head>
<body>
	<?
		global $act;
		partial('zHiddenBlock');
	?>
	<div class="leftMenu-bg"></div>
	<div class="hBasket-inner">
		<div class="hBasket-inner-scroll">
			<?partial('hBasketElems');?>
		</div>
		<div class="hBasket-inner-text">
			В корзине <span>3</span> товара на сумму: <span>3255 р</span>
		</div>
		<div class="hBasket-buttons">
			<a href="#" class="btn btn-md btn-default">Просмотр корзины</a>
			<a href="#" class="btn btn-md btn-primary">ОФОРМЛЕНИЕ ЗАКАЗА</a>
			<div class="juster"></div>
		</div>
	</div>
	<header class="header">
		<div class="header-top">
			<div class="container">
				<div class="header-links">
					<a href="#registerForm" class="fancybox-popup"><span class="icon sprite sprite-icon_key"><img src="images/icon_key.png" alt=""></span>Регистрация</a>
					<div class="hsep"></div>
					<a href="#loginForm" class="fancybox-popup"><span class="icon sprite sprite-icon_hum"><img src="images/icon_hum.png" alt=""></span>Войти</a>
				</div>
			</div>
		</div>
		<div class="header-bottom">
			<div class="container">
				<div class="header-right">
					<div class="hBasket">
						<div class="hBasket-top">
							<a href="#">
								<div class="hBasket-img">
									<img src="images/basket.png" alt=""/>
								</div>
								<div class="hBasket-text">
									пусто :(
								</div>
							</a>
						</div>
					</div>
					<div class="hPhone">
						(495)755-45-10
					</div>
					<div class="hTime">
						ПН-ПТ с  9.30 до 23.00
					</div>
					<a href="#recallForm" class="button btn-info fancybox-popup">
						<span class="icon sprite-icon_phonew"><img src="images/icon_phonew.png" alt=""/></span> Заказать звонок
					</a>
				</div>
				<div class="hSlider">
					<div class="hSlider-item">
						<div class="hSlider-item-img">
							<img src="images/slider.png" alt=""/>
						</div>
						<div class="hSlider-item-text">
							<div class="hSlider-item-text-inner">
								<h3>Алтайские раки</h3>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ullam dolores eaque molestiae nobis enim, non inventore voluptatem corrupti alias voluptas provident nesciunt sed, deleniti similique reiciendis iste, eos maiores reprehenderit quibusdam! Necessitatibus ad, distinctio a, aliquam corrupti voluptatum molestiae ducimus, inventore quae, aut quia magni. Perferendis doloribus, recusandae deleniti ipsam quia, vel ea ipsum nisi.</p>
							</div>
							<a href="#" class="btn btn-primary">Заказать</a>
						</div>
					</div>
					<div class="hSlider-item">
						<div class="hSlider-item">
							<div class="hSlider-item-img">
								<img src="images/slider.png" alt=""/>
							</div>
							<div class="hSlider-item-text">
								<div class="hSlider-item-text-inner">
									<h3>Алтайские раки</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ullam dolores eaque molestiae nobis enim, non inventore voluptatem corrupti alias voluptas provident nesciunt sed, deleniti similique reiciendis iste, eos maiores reprehenderit quibusdam! Necessitatibus ad, distinctio a, aliquam corrupti voluptatum molestiae ducimus, inventore quae, aut quia magni. Perferendis doloribus, recusandae deleniti ipsam quia, vel ea ipsum nisi.</p>
								</div>
								<a href="#" class="btn btn-primary">Заказать</a>
							</div>
						</div>
					</div>
					<div class="hSlider-item">
						<div class="hSlider-item">
							<div class="hSlider-item-img">
								<img src="images/slider.png" alt=""/>
							</div>
							<div class="hSlider-item-text">
								<div class="hSlider-item-text-inner">
									<h3>Алтайские раки</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ullam dolores eaque molestiae nobis enim, non inventore voluptatem corrupti alias voluptas provident nesciunt sed, deleniti similique reiciendis iste, eos maiores reprehenderit quibusdam! Necessitatibus ad, distinctio a, aliquam corrupti voluptatum molestiae ducimus, inventore quae, aut quia magni. Perferendis doloribus, recusandae deleniti ipsam quia, vel ea ipsum nisi.</p>
								</div>
								<a href="#" class="btn btn-primary">Заказать</a>
							</div>
						</div>
					</div>
					<div class="hSlider-item">
						<div class="hSlider-item">
							<div class="hSlider-item-img">
								<img src="images/slider.png" alt=""/>
							</div>
							<div class="hSlider-item-text">
								<div class="hSlider-item-text-inner">
									<h3>Алтайские раки</h3>
								<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat ullam dolores eaque molestiae nobis enim, non inventore voluptatem corrupti alias voluptas provident nesciunt sed, deleniti similique reiciendis iste, eos maiores reprehenderit quibusdam! Necessitatibus ad, distinctio a, aliquam corrupti voluptatum molestiae ducimus, inventore quae, aut quia magni. Perferendis doloribus, recusandae deleniti ipsam quia, vel ea ipsum nisi.</p>
								</div>
								<a href="#" class="btn btn-primary">Заказать</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>