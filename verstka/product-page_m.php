	<?include 'header_mobile.php';?>
	<div class="productPage innerPage">
		<div class="container main">
			<div class="row">
				<div class="content_w">
					<div class="content">
						<div class="clearfix"></div>
						<div class="productTop">
							<div class="productDescription-title">
								<h2>
									КРАСНАЯ ИКРА КЕТЫ "ИТА СЕВЕРНАЯ КОМПАНИЯ" СТ\Б, 114г.
								</h2>
							</div>
							<div class="productTop-left">
								<?partial('productGallery_m');?>
							</div>
							<div class="productTop-right productPrice">
								<?partial('productDescription_m');?>
							</div>
						</div>
						<div class="thirdheading">
							<span>ИНФОРМАЦИЯ О ТОВАРЕ:</span>
						</div>
						<?partial('infoProduct_m');?>
						<div class="thirdheading">
							<span>РЕКОМЕНДУЕМЫЕ ТОВАРЫ</span>
						</div>
						<div class="prodCarousel">
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
							<div class="prodCarouselitem">
								<?partial('productItem');?>
								<?partial('productItem');?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>