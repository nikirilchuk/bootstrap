	<?include 'header.php';?>
	<div class="contactsPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							НОВОСТИ
						</h3>
						<?partial('ctextBlock');?>
						<div class="thirdheading">
							<span>КОММЕНТАРИИ</span>
						</div>
						<?partial('reviewBlock');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>