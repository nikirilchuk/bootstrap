		<div class="footer_placeholder"></div>
		<footer class="footer">
			<div class="footer-left">
				<div class="fSocial">
					<div class="fSocial-title">МЫ В СЕТИ:</div>
					<div class="fSocial-icons">
						<a href="#" class="icon icon-mob icon-soc_tv"></a>
						<a href="#" class="icon icon-mob icon-soc_vk"></a>
						<a href="#" class="icon icon-mob icon-soc_fb"></a>
						<a href="#" class="icon icon-mob icon-soc_gp"></a>
						<a href="#" class="icon icon-mob icon-soc_od"></a>
						<a href="#" class="icon icon-mob icon-soc_inst"></a>
					</div>
				</div>
			</div>
			<div class="footer-right">
				<ul class="footer-menu">
					<li><a href="#"><span class="icon icon-mob icon_fmenu1"></span>Доставка</a></li>
					<li><a href="#"><span class="icon icon-mob icon_fmenu2"></span>Рецепты</a></li>
					<li><a href="#"><span class="icon icon-mob icon_fmenu3"></span>Видео</a></li>
					<li><a href="#"><span class="icon icon-mob icon_fmenu4"></span>Новости</a></li>
					<li><a href="#"><span class="icon icon-mob icon_fmenu5"></span>Контакты</a></li>
					<li><a href="#"><span class="icon icon-mob icon_fmenu6"></span>О нас</a></li>
				</ul>
				<div class="fContacts">
					<div class="fContacts-row">
						<i class="icon icon-mob icon_fphone"></i>(495)755-45-10
					</div>
					<div class="fContacts-row">
						<a href="#"><i class="icon icon-mob icon_fmess"></i>info@shop.rusfds.ru</a>
					</div>
				</div>
			</div>
		</footer>
	</div>
	<?partial('head_mobile')?>
</body>
</html>