	<?include 'header_mobile.php';?>
	<div class="registerPage innerPage">
		<div class="container main">
			<div class="row">
				<div class="innerContent">
					<h3 class="text-center">
						РЕГИСТРАЦИЯ
					</h3>
					<div class="zForm checkout checkoutStep-two zNice">
						<form action="ajax.php">
							<div class="zForm-row">
								<div class="zForm-title">ЛИЧНЫЕ ДАННЫЕ</div>
								<div class="zForm-inner">
									<div class="zForm-row">
										<input type="text" name="text1" placeholder="Имя, Отчество" required="required"/>
									</div>
									<div class="zForm-row">
										<input type="text" name="text2" placeholder="Фамилия" required="required"/>
									</div>
									<div class="zForm-row">
										<input type="email" name="email" placeholder="E-Mail" required="required"/>
									</div>
									<div class="zForm-row">
										<input type="text" name="email" class="phoneMask" placeholder="Телефон" required="required"/>
									</div>
								</div>
								<div class="zForm-title">Адрес</div>
								<div class="zForm-inner">
									<div class="zForm-row">
										<input type="text" name="address" placeholder="Регион / Область:" required="required"/>
									</div>
									<div class="zForm-row">
										<input type="text" name="address2" placeholder="Адрес" required="required"/>
									</div>
								</div>
								<div class="zForm-title">ВАШ ПАРОЛЬ</div>
								<div class="zForm-inner passwordForm">
									<div class="zForm-row">
										<input type="password" name="pass1" id="passwordo" placeholder="* Пароль:" required="required"/>
									</div>
									<div class="zForm-row">
										<input type="password" name="pass2" id="re-passwordo" placeholder="* Повторите пароль:" required="required"/>
									</div>
								</div>
								<div class="zForm-title">РАССЫЛКА НОВОСТЕЙ</div>
								<div class="zForm-subtitle">Подписка на новости:</div>
								<div class="zForm-row text-center checkboxsesForm">
									<label>
										<input type="radio" name="checradio"  checked="checked"/> <span class="zForm-text">Да </span>
									</label>

									<label>
										<input type="radio" name="checradio" /> <span class="zForm-text">Нет  </span>
									</label>
								</div>
							</div>
							<div class="zForm-row text-center">
								<input type="submit" class="btn-primary btn-lg" value="Зарегистрироваться" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>