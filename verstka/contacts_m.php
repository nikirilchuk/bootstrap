	<?include 'header_mobile.php';?>
	<div class="contactsPage innerPage">
		<div class="container main">
			<div class="row">
				<h3 class="text-center">
					НАШЕ МЕСТОНАХОЖДЕНИЕ
				</h3>
				<div class="contactsPage-info">
					<div class="contactsPage-row">
						<div class="icon icon-mob icon_pic"></div>
						<div class="contactsPage-info-text">Морские деликатесы. <br /> г.Москва, ул.Боровая д.10 стр.2</div>
					</div>
					<div class="contactsPage-row phoneText">
						<div class="icon icon-mob icon_bphone"></div>
						<div class="contactsPage-info-text">+7(495)755-45-10</div>
					</div>
				</div>
				<div class="contactsPage-map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2244.2853547978293!2d37.714040600000004!3d55.77091570000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b5355d5fefe4ab%3A0x736188e2334fd34a!2z0JHQvtGA0L7QstCw0Y8g0YPQuy4sIDEw0LoyLCDQnNC-0YHQutCy0LAsINCg0L7RgdGB0LjRjywgMTExMDIw!5e0!3m2!1sru!2sua!4v1442428321180" width="100%" height="325" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="zForm zNice jsForm">
					<div class="innerContent">
						<div class="zForm-title">
							НАПИСАТЬ НАМ
						</div>
						<form action="ajax.js">
							<div class="completePopup-text" data-title="Спасибо за вопрос" data-text="Мы ответим вам в ближайшее время"></div>
							<div class="zForm-inner">
								<div class="zForm-row">
									<input type="text" name="text1" placeholder="Ваше имя" required="required"/>
								</div>
								<div class="zForm-row">
									<input type="email" name="email" placeholder="Ваш e-mail" required="required"/>
								</div>
								<div class="zForm-row">
									<textarea name="texto" rows="5" placeholder="Ваш вопрос" required="required"></textarea>
								</div>
								<div class="zForm-row">
									<input type="submit" class="btn-primary btn-md" value="Отправить" />
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="innerContent">
					<div class="contactsPage-text ctext">
						<h4>НАШИ РЕКВИЗИТЫ</h4>
						<ul>
							<li>АО «АЛЬФА-БАНК» ИНН 7728168971</li>
							<li>КПП по месту нахождения 775001001</li>
							<li>КПП крупнейшего налогоплательщика 997950001</li>
							<li>Кор/сч. 30101810200000000593 в ГУ Банка России по ЦФО</li>
							<li>БИК 044525593</li>
							<li>Код ОКПО: 09610444</li>
							<li>Код ОКАТО: 45286565000</li>
							<li>Код ОКТМО: 45378000000</li>
							<li>Код ОКВЭД: 65.12</li>
							<li>Код ОГРН: 1027700067328</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer_mobile.php';?>