	<?include 'header.php';?>
	<div class="contactsPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu2');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							ИКРА КЕТЫ
						</h3>
						<div class="thirdheading">
							<span>Фильтр</span>
							<a href="#" class="toogleBlock" data-block="productsFilter">Скрыть</a>
						</div>
						<?partial('product-filter');?>
						<?partial('productList-item');?>
						<div class="paginator-center">
							<?partial('paginator');?>
						</div>
						<?partial('productList-Text');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>