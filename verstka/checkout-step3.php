<?include 'header.php';?>
<div class="checkoutPage innerPage">
	<div class="container main">
		<div class="row">
			<?partial('leftMenu');?>
			<div class="content_w">
				<div class="content">
					<?partial('breadcrumbs');?>
					<h3 class="text-center">
						ОФОРМЛЕНИЕ ЗАКАЗА
					</h3>
					<div class="zForm checkout checkoutStep-tree zNice">
						<div class="zForm-head">
							Шаг 3: Способ оплаты
						</div>
						<form action="ajax.php">
							<div class="zForm-title">Выберите способ оплаты для этого заказа:</div>
							<div class="zForm-inner mlForm">
								<div class="zForm-table">
									<div class="zForm-table-col">
										<label><input type="radio" name="checradio" checked="checked"/> <span class="zForm-text">Оплата при доставке </span></label>
									</div>
									<div class="zForm-table-col">
										<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images/pay_img1.png" alt=""/> </span></label>
									</div>
									<div class="zForm-table-col">
										<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images/pay_img2.png" alt=""/>  </span></label>
									</div>
									<div class="zForm-table-col">
										<label><input type="radio" name="checradio" /> <span class="zForm-text"><img src="images/pay_img3.png" alt=""/>  </span></label>
									</div>
								</div>
							</div>
							<div class="zForm-subtitle">Вы можете добавить комментарий к своему заказу:</div>
							<div class="zForm-row">
								<textarea rows="2" name="text-2" required="required"></textarea>
							</div>
							<div class="zForm-row text-right buttonRow">
								<input type="submit" class="btn-primary btn-md" value="Продолжить" />
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?include 'footer.php';?>