	<?include 'header.php';?>
	<div class="videosPage innerPage">
		<div class="container main">
			<div class="row">
				<?partial('leftMenu');?>
				<div class="content_w">
					<div class="content">
						<?partial('breadcrumbs');?>
						<h3 class="text-center">
							Видео
						</h3>
						<div class="videosPage-items">
							<div class="mgBlock">
								<div class="thirdheading">
									<span>РЕЦЕПТЫ</span><a href="#">Все видео</a>
								</div>
								<?partial('video-items');?>
							</div>
							<div class="mgBlock">
								<div class="thirdheading">
									<span>ОТЗЫВЫ</span><a href="#">Все видео</a>
								</div>
								<?partial('video-items');?>
							</div>
							<div class="mgBlock">
								<div class="thirdheading">
									<span>ТОВАРЫ</span><a href="#">Все видео</a>
								</div>
								<?partial('video-items');?>
							</div>
							<div class="mgBlock">
								<div class="thirdheading">
									<span>КОНКУРСЫ</span><a href="#">Все видео</a>
								</div>
								<?partial('video-items');?>
							</div>
							<div class="mgBlock">
								<div class="thirdheading">
									<span>СРАВНЕНИЕ ПРОДУКЦИИ</span><a href="#">Все видео</a>
								</div>
								<?partial('video-items');?>
							</div>
						</div>
						<?partial('paginator');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>