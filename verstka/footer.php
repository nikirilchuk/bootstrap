	<div class="footer_placeholder"></div>
	<footer class="footer">
		<div class="container">
			<div class="leftMenu_w">
				<div class="fSocial">
					<div class="fSocial-title">МЫ В СЕТИ:</div>
					<div class="fSocial-icons">
						<a href="#" class="icon sprite-icon_tv_gr"><img src="images/fsoc1.png" alt=""/></a>
						<a href="#" class="icon sprite-icon_vk_gr"><img src="images/fsoc2.png" alt=""/></a>
						<a href="#" class="icon sprite-icon_fb_gr"><img src="images/fsoc3.png" alt=""/></a>
						<a href="#" class="icon sprite-icon_gp_gr"><img src="images/fsoc4.png" alt=""/></a>
						<a href="#" class="icon sprite-icon_od_gr"><img src="images/fsoc5.png" alt=""/></a>
						<a href="#" class="icon sprite-icon_inst_gr"><img src="images/fsoc6.png" alt=""/></a>
					</div>
				</div>
			</div>
			<div class="content_w">
				<div class="content">
					<ul class="footer-menu">
						<li><a href="#"><span class="icon sprite-icon_deliv"><img src="images/icon_deliv.png" alt=""/></span>Доставка</a></li>
						<li><a href="#"><span class="icon sprite-icon_food_lit"><img src="images/icon_food_lit.png" alt=""/></span>Рецепты</a></li>
						<li><a href="#"><span class="icon sprite-icon_cam_lit"><img src="images/icon_cam_lit.png" alt=""/></span>Видео</a></li>
						<li><a href="#"><span class="icon sprite-icon_list_lit"><img src="images/icon_list_lit.png" alt=""/></span>Новости</a></li>
						<li><a href="#"><span class="icon sprite-icon_pic"><img src="images/icon_pic.png" alt=""/></span>Контакты</a></li>
						<li><a href="#"><span class="icon sprite-icon_prof"><img src="images/icon_prof.png" alt=""/></span>О нас</a></li>
					</ul>
					<div class="footer-bottom">
						<div class="footer-right">
							<div class="fContacts">
								<div class="fContacts-row">
									<i class="icon sprite-icon_phone_gr"><img src="images/icon_phone_gr.png" alt=""/></i>(495)755-45-10
								</div>
								<div class="fContacts-row">
									<a href="#"><i class="icon sprite-icon_mess_gr"><img src="images/icon_mess_gr.png" alt=""/></i>info@shop.rusfds.ru</a>
								</div>
							</div>
						</div>
						<div class="footer-left">
							<div class="yandexReting">
								<a href="#"><img src="images/retingya.jpg" alt="" /></a>
							</div>
						</div>
						<div class="footer-center">
							<div class="payInfo">
								<div class="payInfo-title">Способы оплаты:</div>
								<img src="images/pay.png" alt="" />
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<?partial('head_bootstrap')?>
</body>
</html>