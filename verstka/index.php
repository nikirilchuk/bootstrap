	<?include 'header.php';?>
	<div class="container main">
		<div class="row">
			<?partial('leftMenu');?>
			<div class="content_w">
				<div class="content">
					<div class="thirdheading">
						<span>ТОВАРЫ НЕДЕЛИ</span><a href="#">Все товары</a>
					</div>
					<div class="prodCarousel">
						<div class="prodCarouselitem">
							<?partial('productItem01');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem02');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem03');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
					</div>
					<div class="thirdheading">
						<span>АКЦИИ</span><a href="#">Все акции</a>
					</div>
					<div class="prodCarousel">
						<div class="prodCarouselitem">
							<?partial('productItem01');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem02');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem03');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
						<div class="prodCarouselitem">
							<?partial('productItem');?>
						</div>
					</div>
					<div class="infoBlocks">
						<div class="infoBlocks-left">
							<div class="thirdheading">
								<span>НОВОСТИ</span><a href="#">Все новости</a>
							</div>
							<div class="newsBlock">
								<div class="newsBlock-col">
									<?partial('newsBlock-item')?>
								</div>
								<div class="newsBlock-col">
									<?partial('newsBlock-item')?>
								</div>
							</div>
						</div>
						<div class="infoBlocks-right">
							<div class="thirdheading">
								<span>РЕЦЕПТЫ</span><a href="#">Все рецепты</a>
							</div>
							<div class="recBlock">
								<?partial('receptItem')?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<?include 'footer.php';?>