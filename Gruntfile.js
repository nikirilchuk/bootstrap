/***
**To run Grunt you need node.js and grunt.
**To install grunt, type in console "npm install -g grunt-cli"
**If dir "node_modules" is absent, to install packages type in console "npm install"
**To put together scripts type in console "grunt scripts".
***/

module.exports = function(grunt){
	grunt.initConfig({ 
		pkg: grunt.file.readJSON('package.json'),
		concat:{
			options:{
				separator: ';\n'
			},
			dist:{
				src: [
					'verstka/js/jquery.min.js', 
					'verstka/js/jquery.browser.min.js',
					'verstka/js/bootstrap/bootstrap.min.js',
					'verstka/js/selectivizr-min.js', 
					'verstka/js/jquery.watermark.min.js',
					'verstka/js/jquery.columnizer.js',
					'verstka/fancybox/jquery.fancybox.js',
					'verstka/js/jquery.validate.min.js', 
					'verstka/js/jquery.znice.validate.js', 
					'verstka/js/jquery.znice.js', 
					'verstka/js/modernizr.js',
					'verstka/js/maskedinput.js',
					'verstka/js/slick.js',
					'verstka/js/flipclock.min.js',
					'verstka/js/scr.js',
				 ],
				dest: 'verstka/js/general.js'
			}
		},
		uglify:{
			js:{
				files:{
					'verstka/js/general.js': ['verstka/js/general.js']
				}
			}
		}
	});

	// grunt.initConfig({ 
	// 	pkg: grunt.file.readJSON('package.json'),
	// 	concat:{
	// 		options:{
	// 			separator: ';\n'
	// 		},
	// 		dist:{
	// 			src: [
	// 				'verstka/js_m/jquery.min.js', 
	// 				'verstka/js_m/jquery.browser.min.js',
	// 				'verstka/js_m/bootstrap/bootstrap.min.js',
	// 				'verstka/js_m/jquery.watermark.min.js',
	// 				'verstka/fancybox/jquery.fancybox.js',
	// 				'verstka/js_m/jquery.validate.min.js', 
	// 				'verstka/js_m/jquery.znice.validate.js', 
	// 				'verstka/js_m/jquery.znice.js', 
	// 				'verstka/js_m/maskedinput.js',
	// 				'verstka/js_m/jquery.mobile.custom.min.js',
	// 				'verstka/js_m/slick.js',
	// 				'verstka/js_m/scr.js',
	// 			 ],
	// 			dest: 'verstka/js_m/general.js'
	// 		}
	// 	},
	// 	uglify:{
	// 		js:{
	// 			files:{
	// 				'verstka/js_m/general.js': ['verstka/js_m/general.js']
	// 			}
	// 		}
	// 	}
	// });
	
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	
	grunt.registerTask('default', ['concat', 'uglify']);
};